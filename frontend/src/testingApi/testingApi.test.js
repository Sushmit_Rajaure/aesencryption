import getuserName from './testingApi'
import axios from 'axios'

jest.mock('axios')

test('returns correct username from cloud', async () => {
    axios.get.mockResolvedValue({
        data: [
            {
                id: 1,
                userName: 'sushmites'
            }
        ]
    })
    const name = await getuserName();
    expect(name).toEqual('sushmites')
})