import axios from 'axios';
import { LocalURL } from '../config';
import CryptoJs from 'crypto-js';
import encryption from '../utils/encryption'

async function getuserName() {
    const response = await axios.get(LocalURL);
    return response.data[0].userName;
}

export default getuserName;
