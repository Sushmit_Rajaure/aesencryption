import aesjs from 'aes-js';

const encryption = function (password) {
  var key = 'aPdSgVkYp3s6v9y/';
  var iv = '/A?D(G+KbPeShVkY';
  var constString = 'coke2_';
  key = aesjs.utils.utf8.toBytes(key);
  iv = aesjs.utils.utf8.toBytes(iv);

  let BearerToken = constString + password;
  let textBytes = aesjs.utils.utf8.toBytes(BearerToken);

  var aesCbc = new aesjs.ModeOfOperation.cbc(key, iv);
  var encryptedBytes = aesCbc.encrypt(textBytes);

  return aesjs.utils.hex.fromBytes(encryptedBytes);
};

export default encryption;
