import React from 'react'
import encryption from './encryption'
import '@testing-library/jest-dom'



test('Returns correct CipherText', () => {
    expect(encryption('9813058480')).toBe('8f594532a610bbfb4e1176f36e0abcba')
})