import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import TestRenderer from 'react-test-renderer'


test('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div)
})

test('Matches Snapshot', () => {
    const tree = TestRenderer.create(
        <App />
    ).toJSON();
    expect(tree).toMatchSnapshot();
})

