import React, { Component } from 'react';
import axios from 'axios';
import CryptoJs from 'crypto-js';
import encryption from './utils/encryption';
import { LocalURL, herokuURL } from './config';

class App extends Component {
  constructor() {
    super();
    this.state = {
      item: '',
      password: '',
      username: '',
      accountnumber: '',
      date: '',
      userid: '',
      phonenumber: '',
    };
    this.changeHandler = this.changeHandler.bind(this);
    this.request = this.request.bind(this);
  }

  changeHandler() {
    this.setState({
      password: document.querySelector('.password').value,
    });
  }

  request() {
    let passkey = this.state.password;
    if (passkey.length === 10) {
      axios
        .get(herokuURL, {
          headers: {
            'Authorization': `${encryption(`${passkey}`)}`,
          },
        })
        .then((response) => {
          document.querySelector('.result').style.display = 'block';
          console.log(response);
          var bytes = CryptoJs.AES.decrypt(response.data, 'secretkey123');
          var decryptedData = JSON.parse(bytes.toString(CryptoJs.enc.Utf8));
          this.setState({
            username: decryptedData.username,
            accountnumber: decryptedData.accountnumber,
            date: decryptedData.subscribeDate,
            userid: decryptedData.userid,
            phonenumber: decryptedData.phonenumber,
          });
        })
        .catch((err) => {
          alert('Data not found');
          console.log(err);
        });
    } else {
      alert('Please Enter valid Phone number');
    }
  }

  render() {
    return (
      <div style={{ textAlign: 'center' }}>
        <h3>Enter Password To Get Data </h3>
        <hr />
        <div>
          {' '}
          <input
            type="password"
            onChange={this.changeHandler}
            value={this.state.password}
            className="password"
            placeholder="Enter Password"
          ></input>
          <button onClick={this.request}>Get Data</button>
        </div>{' '}
        <br />
        <div
          className="result"
          style={{ textAlign: 'left', display: 'none', textAlign: 'center' }}
        >
          {' '}
          <div>
            {' '}
            Username: {this.state.username} <br /> UserID: {this.state.userid}{' '}
            <br /> Account Number: {this.state.accountnumber} <br />
            Phone Number: {this.state.phonenumber} <br /> Joined Date:{' '}
            {this.state.date}{' '}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
