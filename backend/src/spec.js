const decryption = require('../utils/decryption')

const request = require('supertest')
const app = require('../app')

describe("GET /", () => {
    test("It should respond with some data", async () => {
        const response = await request(app).get("/bank")
            .set({ 'Authorization': '8f594532a610bbfb4e1176f36e0abcba' })
        expect(response.statusCode).toBe(200);
    })
})

describe("POST /", () => {
    test("Should respond with status 200", async () => {
        const response = await request(app).post("/bank")
            .send({
                "username": "Romanov",
                "userid": 24,
                "accountnumber": 381001755328,
                "phonenumber": 9813051238
            })
        expect(response.statusCode).toBe(201)
    })
})


describe('return Cipher text', () => {
    it('Returns correct deciphered TestCase', () => {
        expect(decryption('8f594532a610bbfb4e1176f36e0abcba')).toBe('coke2_9813058480')
    })
})


describe('My Test Suite', () => {
    it('My Test Case', () => {
        expect(true).toEqual(true);
    });
});