const mongoose = require('mongoose');

const BankSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
    },
    userid: {
        type: Number,
        required: true,
    },
    accountnumber: {
        type: Number,
        required: true
    },
    phonenumber: {
        type: Number,
        required: true
    },
    subscribeDate: {
        type: Date,
        required: true,
        default: Date.now,
    },
});

module.exports = mongoose.model('BankCustomer', BankSchema);