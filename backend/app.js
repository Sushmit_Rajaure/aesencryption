require('dotenv').config();
const cors = require('cors');

const express = require('express');
const app = express();
app.use(cors());
const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URI || process.env.DATABASE_URI, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
});

const PORT = process.env.PORT || 8080;

const db = mongoose.connection;
db.on('error', (error) => console.log(error));
db.once('open', () => console.log('Connected to database'));

app.use(express.json());


const bankRouter = require('./routes/bankdetails')

app.use('/bank', bankRouter)

module.exports = app