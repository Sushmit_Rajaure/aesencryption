const express = require('express');
const router = express.Router();
const Subscriber = require('../models/subscriber');

const aesjs = require('aes-js')

const decryption = function (key, iv, encryptedHex) {
  key = aesjs.utils.utf8.toBytes(key);
  iv = aesjs.utils.utf8.toBytes(iv);

  var encryptedBytes = aesjs.utils.hex.toBytes(encryptedHex);

  var aesCbc = new aesjs.ModeOfOperation.cbc(key, iv);
  var decryptedBytes = aesCbc.decrypt(encryptedBytes);

  return aesjs.utils.utf8.fromBytes(decryptedBytes);

}



//Getting all
router.get('/', async (req, res) => {
  res.header('Access-Control-Expose-Headers', 'Authorization');

  if (req.headers.authorization) {
    var password = decryption('aPdSgVkYp3s6v9y/', '/A?D(G+KbPeShVkY', `${req.headers.authorization}`);

  } else res.status(401).statusMessage('Wrong Password')
  try {
    res.setHeader('Authorization', 'password123');
    const subscribers = await Subscriber.find();

    res.json(subscribers);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

//Getting One
router.get('/:id', getSubscriber, (req, res) => {
  res.json(res.subscriber);
});

//Creating One
router.post('/', async (req, res) => {
  console.log('Post Initiated');
  const subscriber = new Subscriber({
    name: req.body.name,
    subscribedToChannel: req.body.subscribedToChannel,
  });
  try {
    const newSubscriber = await subscriber.save();
    res.status(201).json(newSubscriber);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

//Updating One
router.patch('/:id', getSubscriber, async (req, res) => {
  if (req.body.name != null) {
    res.subscriber.name = req.body.name;
  }
  if (req.body.subscribedToChannel != null) {
    res.subscriber.subscribedToChannel = req.body.subscribedToChannel;
  }
  try {
    const updatedSubscriber = await res.subscriber.save();
    res.json(updatedSubscriber);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

//Deleting One
router.delete('/:id', getSubscriber, async (req, res) => {
  try {
    await res.subscriber.remove();
  } catch (err) {
    res.status(500).json({
      message: 'Error Deleting',
    });
  }
});

async function getSubscriber(req, res, next) {
  let subscriber;
  try {
    subscriber = await Subscriber.findById(req.params.id);
    if (subscriber == null) {
      return res.status(404).json({ message: 'Cannot find subscriber' });
    }
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
  res.subscriber = subscriber;
  next();
}

module.exports = router;

// res.send([
//   {
//     name: 'Sushmit',
//     lastname: 'Rajaure',
//     education: 'Bachelors',
//   },
//   {
//     name: 'Ashmit',
//     lastname: 'Rajaure',
//     education: 'Bachelors',
//   },
// ]);

// try {
//   const subscribers = await Subscriber.find();
//   // const encrypted = aes256.encrypt(key, subscribers);
//   // const decrypted = aes256.decrypt(req.params.pw, encrypted);
//   res.json(subscribers);
// } catch (err) {
//   res.status(500).json({ message: err.message });
// }

// const encrypted = aes256.encrypt(key, subscribers);
// const decrypted = aes256.decrypt(req.params.pw, encrypted);
