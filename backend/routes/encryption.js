const crypto = require('crypto');
module.exports = function encrypt(payload, privateKey) {
    const aesKey = crypto.randomBytes(16);
    const iv = new Buffer.alloc("");
    const cipher = crypto.createCipheriv('aes-128-ecb', aesKey, iv);
    var encryptedPayload = cipher.update(new Buffer.alloc(payload, 'utf8'), 'utf8', 'base64');
    const securePayload = encryptedPayload + cipher.final('base64');
    const secureKey = crypto.privateEncrypt(privateKey, aesKey).toString('base64');
    return {
        securePayload: securePayload,
        secureKey: secureKey
    };
};