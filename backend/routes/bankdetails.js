const express = require('express');
const router = express.Router();
const bankDetails = require('../models/bankDetails');
const CryptoJs = require("crypto-js")
const decryption = require('../utils/decryption')


//Getting all
router.get('/', async (req, res) => {
    try {
        var receivedPass = decryption(req.headers.authorization)
        userNumber = parseInt(receivedPass.split('coke2_')[1])
        const bankers = await bankDetails.find()
        console.log(bankers)
        var checkingDataForNumber = bankers.find(user => user.phonenumber === userNumber)
        var ciphertext = CryptoJs.AES.encrypt(JSON.stringify(checkingDataForNumber), 'secretkey123').toString()
        if (checkingDataForNumber) {
            res.send(ciphertext)
        } else res.status(401).json({ message: 'User Not Found' })
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

//Deleting One
router.delete('/:id', getBankDetails, async (req, res) => {
    try {
        await res.bankDetails.remove();
    } catch (err) {
        res.status(500).json({
            message: 'Error Deleting',
        });
    }
});


//Creating One
router.post('/', async (req, res) => {
    console.log('Bank Post Initiated');
    const bankdetails = new bankDetails({
        username: req.body.username,
        userid: req.body.userid,
        accountnumber: req.body.accountnumber,
        phonenumber: req.body.phonenumber
    });
    try {
        const newbankdetails = await bankdetails.save();
        res.status(201).json(newbankdetails);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

async function getBankDetails(req, res, next) {
    let banker;
    try {
        banker = await bankDetails.find(req.headers.authorization);
        if (banker == null) {
            return res.status(404).json({ message: 'Cannot find banker' });
        }
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
    res.banker = banker;
    next();
}

module.exports = router;