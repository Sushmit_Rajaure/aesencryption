const aesjs = require('aes-js')

module.exports = function (encryptedHex) {
    var key = 'aPdSgVkYp3s6v9y/';
    var iv = '/A?D(G+KbPeShVkY';
    key = aesjs.utils.utf8.toBytes(key);
    iv = aesjs.utils.utf8.toBytes(iv);

    var encryptedBytes = aesjs.utils.hex.toBytes(encryptedHex);

    var aesCbc = new aesjs.ModeOfOperation.cbc(key, iv);
    var decryptedBytes = aesCbc.decrypt(encryptedBytes);

    return aesjs.utils.utf8.fromBytes(decryptedBytes);

}

