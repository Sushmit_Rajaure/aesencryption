const decryption = require('./decryption')
require('@testing-library/jest-dom')

test('Returns correct deciphered text', () => {
    expect(decryption('8f594532a610bbfb4e1176f36e0abcba')).toBe('coke2_9813058480')
})
